#include <WiFi.h>
#include <HTTPClient.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SharpMem.h>
#include <TaskScheduler.h>
#include <qrcode.h>
#include <ArduinoJson.h>

#include "Config.h"
#include "AirPayLogo.h"
#include "Scheduler.h"
// #include "QRCodeDemo.h"

Scheduler scheduler;
Task taskButton(30, TASK_FOREVER, &buttonCb, &scheduler, true);
Task taskCheckOrder(1000, TASK_FOREVER, &checkOrderCb, &scheduler, false);

Adafruit_SharpMem display(DISPLAY_SCK, DISPLAY_MOSI, DISPLAY_SS, DISPLAY_LH, DISPLAY_LW);

//Variables
String orderId = "";
bool isWifi = false;
int pinEspresso = 32;
bool taskAdded = false;

/**
 * QRCode utility methods.
 */
void genQRAndDraw(const char *text) { // #T>1234567890<
	// Start time
	uint32_t dt = millis();

	// Create the QR code
	QRCode qrcode;
	uint8_t qrcodeData[qrcode_getBufferSize(3)];
	qrcode_initText(&qrcode, qrcodeData, 3, 0, text);

	// Delta time
	dt = millis() - dt;
	Serial.print("QR Code Generation Time: ");
	Serial.print(dt);
	Serial.print("\n");

	uint8_t offsetX = 4;
	uint8_t offsetY = 4;

	clearDisplay(false);

	for (uint8_t y = 0; y < qrcode.size; y++) {
		// Each horizontal module.
		for (uint8_t x = 0; x < qrcode.size; x++) {
			uint8_t axeX = x * 3 + offsetX;
			uint8_t axeY = y * 3 + offsetY;
			uint8_t colour = qrcode_getModule(&qrcode, x, y) ? BLACK : WHITE;

			// For every pixel of the module, I have to draw 9 of them.
			// We're drawing 3 times the size.
			display.drawPixel(axeX, axeY, colour);
			display.drawPixel(axeX + 1, axeY, colour);
			display.drawPixel(axeX + 2, axeY, colour);

			display.drawPixel(axeX, axeY + 1, colour);
			display.drawPixel(axeX + 1, axeY + 1, colour);
			display.drawPixel(axeX + 2, axeY + 1, colour);

			display.drawPixel(axeX, axeY + 2, colour);
			display.drawPixel(axeX + 1, axeY + 2, colour);
			display.drawPixel(axeX + 2, axeY + 2, colour);
		}
	}
	display.refresh();
}

/**
 * Display utility methods.
 */
 // Can only be called if we don't need
 // to draw anything else in the same cycle.
void clearDisplay(bool isInverted) {
	display.clearDisplay();
	display.fillScreen(isInverted ? BLACK : WHITE);
	display.setCursor(0,0);
	display.refresh();
}

void drawText(char* text) {
	display.setTextSize(2);
	display.setTextColor(WHITE);
	display.setCursor(0,0);
	display.println(text);
	display.refresh();
}

void drawAirPayLogo() {
	clearDisplay(true);
	display.fillScreen(BLACK);
	display.drawBitmap(0, 0, AirPayLogo, 96, 96, 1);
	display.refresh();
}


/**
 * HTTPClient utilities.
 */
String getOrderIdAPICall() {
  HTTPClient http;

  String id = "";
  //Configure endpoint
  http.begin("http://18.235.170.58/v1/order?amount=10");
  http.addHeader("Auth","DEADBEEF");

  //Start connection and send header
  int httpCode = http.GET();
  //debug---
  //Serial.println(httpCode);
  if(httpCode > 0) {
	  //Good response found
	  if(httpCode == HTTP_CODE_OK) {
      // Parse JSON object
      String payload = http.getString();
      //Serial.println(payload);
      DynamicJsonBuffer jsonBuffer;
      JsonObject& root = jsonBuffer.parseObject(payload);
      if (!root.success()) {
        Serial.println(F("Parsing failed!"));
      }
      id = root["id"].as<char*>();
      Serial.println(id);
	  }
  }else{
    Serial.println("Order Error...");
  }
  http.end();
  orderId = id;
  return id;
}

bool getCheckStatusOrderAPICall() {
  HTTPClient http;

  bool statusOrder = false;
  String orderStatus = "";
  //Configure endpoint
  String endPoint = "http://18.235.170.58/v1/order/status?order_number=";
  endPoint = endPoint + orderId;
  Serial.println(endPoint);
  http.begin(endPoint);
  http.addHeader("Auth","DEADBEEF");

  //Start connection and send header
  int httpCode = http.GET();
  //debug---
  Serial.println(httpCode);
  if(httpCode > 0) {
    //Good response found
    if(httpCode == HTTP_CODE_OK) {
      // Parse JSON object
      String payload = http.getString();
      //Serial.println(payload);
      DynamicJsonBuffer jsonBuffer;
      JsonObject& root = jsonBuffer.parseObject(payload);
      if (!root.success()) {
        Serial.println(F("Parsing failed!"));
      }
      orderStatus = root["order_status"].as<char*>();
      Serial.println(orderStatus);
      if(orderStatus.equals("ORDER_PAID")){
        Serial.println("SUCCESS -> ORDER PAID -> MAKE A COFFEE!");
        statusOrder = true;
      }
    }
  }else{
    Serial.println("Order Error...");
  }
  http.end();
  
  return statusOrder;
}


/**
 * TaskScheduler methods.
 */
 void buttonCb() {
	bool isButton = digitalRead(PIN_BUTTON) == LOW; // Pull up.
	if (isButton) {
		Serial.println("*** BUTTON PRESS ***");
		// TODO: Call APIs to get transaction id.
    //----------------------API CALL GET OERDER------------------------
    String trsFromAPI = getOrderIdAPICall();
    char transactionId[11];
    trsFromAPI.toCharArray(transactionId,11);
    //-----------------------------------------------------------------
		//char transactionId[] = "1234567890";
		char prefix[] = "#T>";
		char postfix[] = "<";
		char qrtext[20] = "";
		strcat(qrtext, prefix);
		strcat(qrtext, transactionId);
		strcat(qrtext, postfix);
		Serial.println("*** Transaction ID ***");
		Serial.println(transactionId);
		// Generate QRCode with the embedded transaction id.
		genQRAndDraw(transactionId);

    //ADD THE TASK TO THE SCHEDULER TO CHECK THE STATUS OF THE ORDER
    if(taskAdded == false){
      taskAdded = true;
      scheduler.addTask(taskCheckOrder);
    }
    taskCheckOrder.enable();
		// Pause reading the button for a second.
		taskButton.delay(1000);
	}
}

//Check the status of an order after its creation
void checkOrderCb(){
  Serial.println("*** CHECK ORDER STATUS ***");
  bool returnedStatus = getCheckStatusOrderAPICall();
  if(returnedStatus == true){
    drawAirPayLogo();
    taskCheckOrder.disable();
    //scheduler.deleteTask(taskCheckOrder);
    digitalWrite(pinEspresso, LOW);
    delay(800);
    digitalWrite(pinEspresso, HIGH);
  }
}


/**
 * SETUP!
 */
void setup() {
	// Debug
	Serial.begin(115200);
	Serial.println("*** Init ***");

	// PINS
	// pinMode(PIN_BUTTON, INPUT_PULLUP);
	// pinMode(LED_BUILTIN, OUTPUT);
  pinMode(pinEspresso, OUTPUT);
  digitalWrite(pinEspresso, LOW);
  delay(500);
  digitalWrite(pinEspresso, HIGH);
  
	// Display init.
	display.begin();
	display.setRotation(DISPLAY_MOD);
	// clearDisplay();
	// drawText(".AirPay.");
	drawAirPayLogo();
	// display.fillScreen(BLACK);
	// display.drawBitmap(0, 0, QR_Code_Demo, 96, 96, 1);
	// display.refresh();

	// WiFi Test
	Serial.println();
	Serial.print("WiFi connecting to ");
	Serial.println(WIFI_SSID);
	// if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
	// 	Serial.println("WiFi failed to configure.");
	// }
	WiFi.mode(WIFI_STA);
	WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
	// Wait for connection.
	while (WiFi.status() != WL_CONNECTED) {
		Serial.print(".");
		delay(500);
	}
	// WiFi is connected.
	isWifi = true;
	Serial.println("");
	Serial.println("WiFi connected");
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());
}


/**
 * LOOP
 */
void loop() {
	// Task Scheduler!
	scheduler.execute();
}
