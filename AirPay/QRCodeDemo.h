const uint8_t QRCodeDemo[] PROGMEM = {
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xe0, 0x00, 0x00, 0x7f, 0xc7, 0x1f, 0xf8, 0x00, 0x7e, 0x00, 0x00, 0x07,
	0xe0, 0x00, 0x00, 0x7f, 0xc7, 0x1f, 0xf8, 0x00, 0x7e, 0x00, 0x00, 0x07,
	0xe0, 0x00, 0x00, 0x7f, 0xc7, 0x1f, 0xf8, 0x00, 0x7e, 0x00, 0x00, 0x07,
	0xe3, 0xff, 0xfc, 0x71, 0xc7, 0xfc, 0x00, 0x1f, 0x8e, 0x3f, 0xff, 0xc7,
	0xe3, 0xff, 0xfc, 0x71, 0xc7, 0xfc, 0x00, 0x1f, 0x8e, 0x3f, 0xff, 0xc7,
	0xe3, 0xff, 0xfc, 0x71, 0xc7, 0xfc, 0x00, 0x1f, 0x8e, 0x3f, 0xff, 0xc7,
	0xe3, 0x80, 0x1c, 0x7f, 0xff, 0x1f, 0xff, 0xff, 0xfe, 0x38, 0x01, 0xc7,
	0xe3, 0x80, 0x1c, 0x7f, 0xff, 0x1f, 0xff, 0xff, 0xfe, 0x38, 0x01, 0xc7,
	0xe3, 0x80, 0x1c, 0x7f, 0xff, 0x1f, 0xff, 0xff, 0xfe, 0x38, 0x01, 0xc7,
	0xe3, 0x80, 0x1c, 0x70, 0x07, 0x00, 0x00, 0x1f, 0xfe, 0x38, 0x01, 0xc7,
	0xe3, 0x80, 0x1c, 0x70, 0x07, 0x00, 0x00, 0x1f, 0xfe, 0x38, 0x01, 0xc7,
	0xe3, 0x80, 0x1c, 0x70, 0x07, 0x00, 0x00, 0x1f, 0xfe, 0x38, 0x01, 0xc7,
	0xe3, 0x80, 0x1c, 0x7e, 0x3f, 0xff, 0xf8, 0xfc, 0x0e, 0x38, 0x01, 0xc7,
	0xe3, 0x80, 0x1c, 0x7e, 0x3f, 0xff, 0xf8, 0xfc, 0x0e, 0x38, 0x01, 0xc7,
	0xe3, 0x80, 0x1c, 0x7e, 0x3f, 0xff, 0xf8, 0xfc, 0x0e, 0x38, 0x01, 0xc7,
	0xe3, 0x80, 0x1c, 0x7e, 0x3f, 0xff, 0xf8, 0xfc, 0x0e, 0x38, 0x01, 0xc7,
	0xe3, 0xff, 0xfc, 0x70, 0x38, 0x00, 0x00, 0xe3, 0xfe, 0x3f, 0xff, 0xc7,
	0xe3, 0xff, 0xfc, 0x70, 0x38, 0x00, 0x00, 0xe3, 0xfe, 0x3f, 0xff, 0xc7,
	0xe3, 0xff, 0xfc, 0x70, 0x38, 0x00, 0x00, 0xe3, 0xfe, 0x3f, 0xff, 0xc7,
	0xe0, 0x00, 0x00, 0x71, 0xc7, 0x1c, 0x38, 0xe3, 0x8e, 0x00, 0x00, 0x07,
	0xe0, 0x00, 0x00, 0x71, 0xc7, 0x1c, 0x38, 0xe3, 0x8e, 0x00, 0x00, 0x07,
	0xe0, 0x00, 0x00, 0x71, 0xc7, 0x1c, 0x38, 0xe3, 0x8e, 0x00, 0x00, 0x07,
	0xff, 0xff, 0xff, 0xfe, 0x00, 0xff, 0xc0, 0xfc, 0x7f, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xfe, 0x00, 0xff, 0xc0, 0xfc, 0x7f, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xfe, 0x00, 0xff, 0xc0, 0xfc, 0x7f, 0xff, 0xff, 0xff,
	0xe0, 0x00, 0x1c, 0x00, 0x38, 0xe0, 0x07, 0x1f, 0x81, 0xc7, 0x0e, 0x3f,
	0xe0, 0x00, 0x1c, 0x00, 0x38, 0xe0, 0x07, 0x1f, 0x81, 0xc7, 0x0e, 0x3f,
	0xe0, 0x00, 0x1c, 0x00, 0x38, 0xe0, 0x07, 0x1f, 0x81, 0xc7, 0x0e, 0x3f,
	0xfc, 0x7e, 0x03, 0xf1, 0xc7, 0x1f, 0xf8, 0xfc, 0x00, 0x00, 0xff, 0xc7,
	0xfc, 0x7e, 0x03, 0xf1, 0xc7, 0x1f, 0xf8, 0xfc, 0x00, 0x00, 0xff, 0xc7,
	0xfc, 0x7e, 0x03, 0xf1, 0xc7, 0x1f, 0xf8, 0xfc, 0x00, 0x00, 0xff, 0xc7,
	0xff, 0xf1, 0xe0, 0x01, 0xc7, 0xe0, 0x07, 0x1f, 0xfe, 0x38, 0xff, 0xff,
	0xff, 0xf1, 0xe0, 0x01, 0xc7, 0xe0, 0x07, 0x1f, 0xfe, 0x38, 0xff, 0xff,
	0xff, 0xf1, 0xe0, 0x01, 0xc7, 0xe0, 0x07, 0x1f, 0xfe, 0x38, 0xff, 0xff,
	0xe3, 0x80, 0x1f, 0xff, 0xff, 0x1c, 0x3f, 0xff, 0x81, 0xf8, 0xfe, 0x3f,
	0xe3, 0x80, 0x1f, 0xff, 0xff, 0x1c, 0x3f, 0xff, 0x81, 0xf8, 0xfe, 0x3f,
	0xe3, 0x80, 0x1f, 0xff, 0xff, 0x1c, 0x3f, 0xff, 0x81, 0xf8, 0xfe, 0x3f,
	0xe3, 0x8f, 0xfc, 0x70, 0x07, 0x00, 0x00, 0x1f, 0x8f, 0xc7, 0x01, 0xff,
	0xe3, 0x8f, 0xfc, 0x70, 0x07, 0x00, 0x00, 0x1f, 0x8f, 0xc7, 0x01, 0xff,
	0xe3, 0x8f, 0xfc, 0x70, 0x07, 0x00, 0x00, 0x1f, 0x8f, 0xc7, 0x01, 0xff,
	0xe0, 0x01, 0xe3, 0xf1, 0xff, 0xe3, 0xf8, 0xe0, 0x00, 0x00, 0xf1, 0xc7,
	0xe0, 0x01, 0xe3, 0xf1, 0xff, 0xe3, 0xf8, 0xe0, 0x00, 0x00, 0xf1, 0xc7,
	0xe0, 0x01, 0xe3, 0xf1, 0xff, 0xe3, 0xf8, 0xe0, 0x00, 0x00, 0xf1, 0xc7,
	0xff, 0xf0, 0x00, 0x00, 0x00, 0x1f, 0xc7, 0xff, 0x8f, 0xc0, 0xf1, 0xff,
	0xff, 0xf0, 0x00, 0x00, 0x00, 0x1f, 0xc7, 0xff, 0x8f, 0xc0, 0xf1, 0xff,
	0xff, 0xf0, 0x00, 0x00, 0x00, 0x1f, 0xc7, 0xff, 0x8f, 0xc0, 0xf1, 0xff,
	0xff, 0xf0, 0x00, 0x00, 0x00, 0x1f, 0xc7, 0xff, 0x8f, 0xc0, 0xf1, 0xff,
	0xe3, 0xf1, 0xff, 0xf0, 0x38, 0xff, 0xc0, 0xfc, 0x0f, 0xc7, 0xfe, 0x3f,
	0xe3, 0xf1, 0xff, 0xf0, 0x38, 0xff, 0xc0, 0xfc, 0x0f, 0xc7, 0xfe, 0x3f,
	0xe3, 0xf1, 0xff, 0xf0, 0x38, 0xff, 0xc0, 0xfc, 0x0f, 0xc7, 0xfe, 0x3f,
	0xff, 0xf0, 0x00, 0x71, 0xc0, 0x1f, 0xc0, 0x1c, 0x71, 0xff, 0xf1, 0xff,
	0xff, 0xf0, 0x00, 0x71, 0xc0, 0x1f, 0xc0, 0x1c, 0x71, 0xff, 0xf1, 0xff,
	0xff, 0xf0, 0x00, 0x71, 0xc0, 0x1f, 0xc0, 0x1c, 0x71, 0xff, 0xf1, 0xff,
	0xe3, 0xff, 0xff, 0x8f, 0xc7, 0x1c, 0x38, 0xfc, 0x70, 0x00, 0x01, 0xc7,
	0xe3, 0xff, 0xff, 0x8f, 0xc7, 0x1c, 0x38, 0xfc, 0x70, 0x00, 0x01, 0xc7,
	0xe3, 0xff, 0xff, 0x8f, 0xc7, 0x1c, 0x38, 0xfc, 0x70, 0x00, 0x01, 0xc7,
	0xe3, 0x80, 0x1c, 0x0f, 0xff, 0x1f, 0xc0, 0x03, 0x8e, 0x00, 0x01, 0xff,
	0xe3, 0x80, 0x1c, 0x0f, 0xff, 0x1f, 0xc0, 0x03, 0x8e, 0x00, 0x01, 0xff,
	0xe3, 0x80, 0x1c, 0x0f, 0xff, 0x1f, 0xc0, 0x03, 0x8e, 0x00, 0x01, 0xff,
	0xe3, 0x80, 0x03, 0xff, 0xc7, 0x1f, 0xc7, 0xe3, 0x80, 0x38, 0xfe, 0x3f,
	0xe3, 0x80, 0x03, 0xff, 0xc7, 0x1f, 0xc7, 0xe3, 0x80, 0x38, 0xfe, 0x3f,
	0xe3, 0x80, 0x03, 0xff, 0xc7, 0x1f, 0xc7, 0xe3, 0x80, 0x38, 0xfe, 0x3f,
	0xe3, 0xf0, 0x00, 0x7e, 0x07, 0x03, 0xf8, 0x1c, 0x00, 0x00, 0xf0, 0x07,
	0xe3, 0xf0, 0x00, 0x7e, 0x07, 0x03, 0xf8, 0x1c, 0x00, 0x00, 0xf0, 0x07,
	0xe3, 0xf0, 0x00, 0x7e, 0x07, 0x03, 0xf8, 0x1c, 0x00, 0x00, 0xf0, 0x07,
	0xff, 0xff, 0xff, 0xf0, 0x3f, 0x1f, 0xf8, 0xff, 0x8f, 0xf8, 0x00, 0x07,
	0xff, 0xff, 0xff, 0xf0, 0x3f, 0x1f, 0xf8, 0xff, 0x8f, 0xf8, 0x00, 0x07,
	0xff, 0xff, 0xff, 0xf0, 0x3f, 0x1f, 0xf8, 0xff, 0x8f, 0xf8, 0x00, 0x07,
	0xe0, 0x00, 0x00, 0x71, 0xc7, 0x1c, 0x00, 0xfc, 0x0e, 0x38, 0x01, 0xff,
	0xe0, 0x00, 0x00, 0x71, 0xc7, 0x1c, 0x00, 0xfc, 0x0e, 0x38, 0x01, 0xff,
	0xe0, 0x00, 0x00, 0x71, 0xc7, 0x1c, 0x00, 0xfc, 0x0e, 0x38, 0x01, 0xff,
	0xe3, 0xff, 0xfc, 0x7f, 0xff, 0x00, 0x07, 0xe0, 0x0f, 0xf8, 0x0e, 0x3f,
	0xe3, 0xff, 0xfc, 0x7f, 0xff, 0x00, 0x07, 0xe0, 0x0f, 0xf8, 0x0e, 0x3f,
	0xe3, 0xff, 0xfc, 0x7f, 0xff, 0x00, 0x07, 0xe0, 0x0f, 0xf8, 0x0e, 0x3f,
	0xe3, 0x80, 0x1c, 0x70, 0x07, 0x03, 0xff, 0x1f, 0x80, 0x00, 0xf0, 0x07,
	0xe3, 0x80, 0x1c, 0x70, 0x07, 0x03, 0xff, 0x1f, 0x80, 0x00, 0xf0, 0x07,
	0xe3, 0x80, 0x1c, 0x70, 0x07, 0x03, 0xff, 0x1f, 0x80, 0x00, 0xf0, 0x07,
	0xe3, 0x80, 0x1c, 0x71, 0xc7, 0x1c, 0x38, 0x1c, 0x0f, 0xff, 0x0e, 0x07,
	0xe3, 0x80, 0x1c, 0x71, 0xc7, 0x1c, 0x38, 0x1c, 0x0f, 0xff, 0x0e, 0x07,
	0xe3, 0x80, 0x1c, 0x71, 0xc7, 0x1c, 0x38, 0x1c, 0x0f, 0xff, 0x0e, 0x07,
	0xe3, 0x80, 0x1c, 0x71, 0xc7, 0x1c, 0x38, 0x1c, 0x0f, 0xff, 0x0e, 0x07,
	0xe3, 0x80, 0x1c, 0x70, 0x38, 0xe3, 0xc0, 0xe0, 0x00, 0x00, 0x0e, 0x3f,
	0xe3, 0x80, 0x1c, 0x70, 0x38, 0xe3, 0xc0, 0xe0, 0x00, 0x00, 0x0e, 0x3f,
	0xe3, 0x80, 0x1c, 0x70, 0x38, 0xe3, 0xc0, 0xe0, 0x00, 0x00, 0x0e, 0x3f,
	0xe3, 0xff, 0xfc, 0x71, 0xff, 0x00, 0x00, 0xff, 0x81, 0xf8, 0x0e, 0x3f,
	0xe3, 0xff, 0xfc, 0x71, 0xff, 0x00, 0x00, 0xff, 0x81, 0xf8, 0x0e, 0x3f,
	0xe3, 0xff, 0xfc, 0x71, 0xff, 0x00, 0x00, 0xff, 0x81, 0xf8, 0x0e, 0x3f,
	0xe0, 0x00, 0x00, 0x70, 0x00, 0x03, 0xc0, 0x1c, 0x7e, 0x3f, 0x01, 0xff,
	0xe0, 0x00, 0x00, 0x70, 0x00, 0x03, 0xc0, 0x1c, 0x7e, 0x3f, 0x01, 0xff,
	0xe0, 0x00, 0x00, 0x70, 0x00, 0x03, 0xc0, 0x1c, 0x7e, 0x3f, 0x01, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
};
