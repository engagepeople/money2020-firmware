#define DEVICE_NAME 	"AirPay"

#define WIFI_SSID 		"Ale's iPhone"
#define WIFI_PASSWORD 	"airplay2018"

#define API_HOST 		""
#define API_PORT 		80

#define PIN_BUTTON 		14

#define DISPLAY_SCK 	12
#define DISPLAY_MOSI 	27
#define DISPLAY_SS 		33
#define DISPLAY_LH		96
#define DISPLAY_LW		96
#define DISPLAY_MOD		2

#define BLACK 			0
#define WHITE			1

#define DEBUG true

IPAddress local_IP(192, 168, 0, 81);
IPAddress gateway(192, 168, 0, 50);
IPAddress subnet(255, 255, 0, 0);
IPAddress primaryDNS(8, 8, 8, 8);
IPAddress secondaryDNS(8, 8, 4, 4);
